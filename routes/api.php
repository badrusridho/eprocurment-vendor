<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\RegistrasiController;
use App\Http\Controllers\Api\GroupMenuController;

// namespace App\Http\Controllers\API;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth/login', [UserController::class, 'loginUser']);

Route::post('/vendor/register', [RegistrasiController::class, 'createVendor']);
Route::post('/vendor/fullregister', [RegistrasiController::class, 'InsertDataVendor']);
Route::post('/vendor/profile', [RegistrasiController::class, 'profileVendor']);
Route::post('/vendor/bank', [RegistrasiController::class, 'bankVendor']);
Route::post('/vendor/izin', [RegistrasiController::class, 'izinVendor']);
Route::post('/vendor/deed', [RegistrasiController::class, 'deedVendor']);
Route::post('/vendor/management', [RegistrasiController::class, 'managementVendor']);
Route::post('/vendor/attachment', [RegistrasiController::class, 'attachmentVendor']);
Route::post('/vendor/ahli', [RegistrasiController::class, 'ahliVendor']);
Route::post('/vendor/SPT', [RegistrasiController::class, 'SPTVendor']);
Route::post('/vendor/experience', [RegistrasiController::class, 'experienceVendor']);

Route::get('/vendor/confirm/{id}', [RegistrasiController::class, 'confirm']);

