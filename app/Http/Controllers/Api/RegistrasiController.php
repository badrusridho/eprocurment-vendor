<?php

namespace App\Http\Controllers\Api;

use App\Models\Vendor;
use App\Models\User;
use App\Mail\sendEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RegistrasiController extends Controller
{
    /**
     * Create User
     * @param Request $request
     * @return User 
     */
    public function confirm($id)
    {
        DB::table('vendor')
                ->where('VENDOR_ID', $id)
                ->update(['CONFIRM' => 1]);
        echo "Konfirmasi Berhasil Silahkan Login Kembali<br>";
    }

    public function createVendor(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(), 
            [
                'USER_NAME' => 'required|unique:vendor,USER_NAME',
                'EMAIL' => 'required|email|unique:vendor,EMAIL',
                'PASSWORD' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $regis = DB::table('vendor')->insert([
                'COMPANY_NAME' => $request->COMPANY_NAME,
                'PASSWORD' => Hash::make($request->PASSWORD),
                'USER_NAME' => $request->USER_NAME,
                'STATUS' => $request->STATUS,
                'NPWP' => $request->NPWP,
                'ADDRESS' => $request->ADDRESS,
                'CITY' => $request->CITY,
                'PROVINCE' => $request->PROVINCE,
                'POSTAL_CODE' => $request->POSTAL_CODE,
                'PHONE' => $request->PHONE,
                'FAX' => $request->FAX,
                'EMAIL' => $request->EMAIL,
                'CONFIRM' => 0
            ]);

            $user = User::create([
                'email' => $request->EMAIL,
                'password' => Hash::make($request->PASSWORD),
                'user_name' => $request->USER_NAME,
                // 'id_card' => $request->NPWP,
                // 'npwp' => $request->NPWP,
                'firstname' => $request->COMPANY_NAME,
                'lastname' => '',
                'company' => $request->COMPANY_NAME,
                'phone' => $request->PHONE,
                'province' => $request->PROVINCE,
                'city' => $request->CITY,
                'address' => $request->ADDRESS,
                'group_users_id' => 2
            ]);

            // dd($dataContact);

            $vendor = DB::table('vendor')->where('USER_NAME', $request->USER_NAME)->get();

            $dataContact=$request->DATACONTACT;
            foreach ($dataContact as $item) {
                // dd($item['contname']);

                $regis = DB::table('contact_number')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'full_name' => $item['contname'],
                    'phone' => $item['contPhone'],
                    'email' => $item['contEmail'],
                    'npwp' => $item['contNPWP'],
                ]);
            }

            if($vendor){
                $email = $request->EMAIL;
                $response = Mail::to($email)->send(new sendEmail($vendor[0]));
                // dd($response);
                if($response)
                {
                    return response()->json([
                        'status' => true,
                        'message' => 'Email sent successfully.',
                    ]);
                }
                else
                {
                    return response()->json([
                        'status' => false,
                        'message' => 'Wrong Data'
                    ]);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Tidak Ditemukan'
                ], 404);
            }

            // return response()->json([
            //     'status' => true,
            //     'message' => 'Vendor Created Successfully',
            //     'token' => $user->createToken("API TOKEN")->plainTextToken
            // ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function profileVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $contact = DB::table('contact_number')
            ->select('contact_number.full_name as contName','contact_number.phone as contPhone',
                    'contact_number.email as contEmail','contact_number.npwp as contNPWP')
            ->where('contact_number.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'data' => $vendor[0],
                    'ContactData' => $contact
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function InsertDataVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.VENDOR_ID')
            ->where('vendor.USER_NAME', $username)
            ->get();

            $DataAttach=$request->DataAttach;
            $DataAhli=$request->DataAhli;
            $DataBank=$request->DataBank;
            $DataIzin=$request->DataIzin;
            $DataDeed=$request->DataDeed;
            $DataExperience=$request->DataExperience;
            $DataManage=$request->DataManage;
            $DataSPT=$request->DataSPT;

            foreach ($DataAttach as $item) {
                $attach = DB::table('attachment')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'attachment_name' => $item['attachment_name'],
                    'attachment_type' => $item['attachment_type'],
                    'exp_date' => $item['exp_date'],
                    'path_file' => $item['path_file'],
                ]);
            }
            
            foreach ($DataAhli as $item) {
                $ahli = DB::table('expert')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'fullname' => $item['fullname'],
                    'dob' => $item['dob']
                ]);
            }
            
            foreach ($DataBank as $item) {
                $ahli = DB::table('bank_account')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'no_rekening' => $item['no_rekening'],
                    'mata_uang' => $item['mata_uang'],
                    'nama_bank' => $item['nama_bank'],
                ]);
            }
            
            foreach ($DataIzin as $item) {
                $ahli = DB::table('business_permit')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'type' => $item['type'],
                    'permit_number' => $item['permit_number'],
                    'start_date' => $item['start_date'],
                    'end_date' => $item['end_date'],
                    'licesnsing_agency' => $item['licesnsing_agency'],
                ]);
            }
            
            foreach ($DataDeed as $item) {
                $ahli = DB::table('deed_of_incorporation')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'deed_no' => $item['deed_no'],
                    'date' => $item['date'],
                    'notary' => $item['notary']
                ]);
            }
            
            foreach ($DataExperience as $item) {
                $ahli = DB::table('exp_vendor')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'job_name' => $item['job_name'],
                    'location' => $item['location'],
                    'company_name' => $item['company_name']
                ]);
            }

            foreach ($DataManage as $item) {
                $ahli = DB::table('management')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'fullname' => $item['fullname'],
                    'idcard' => $item['idcard'],
                    'position' => $item['position']
                ]);
            }
            
            foreach ($DataSPT as $item) {
                $ahli = DB::table('tax')->insert([
                    'vendor_id' => $vendor[0]->VENDOR_ID,
                    'year' => $item['year'],
                    'tax_no' => $item['tax_no'],
                    'date' => $item['date']
                ]);
            }

            if($vendor){
                return response()->json([
                    'status' => true,
                    'message' => 'Data Berhasil Di Simpan',
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Gagal Di Simpan'
                ], 404);
            }

            // return response()->json([
            //     'status' => true,
            //     'message' => 'Vendor Created Successfully',
            //     'token' => $user->createToken("API TOKEN")->plainTextToken
            // ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function bankVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $bank = DB::table('bank_account')
            ->select('bank_account.*')
            ->where('bank_account.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'bankData' => $bank
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function izinVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $izin = DB::table('business_permit')
            ->select('business_permit.*')
            ->where('business_permit.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'izinData' => $izin
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function deedVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $deed = DB::table('deed_of_incorporation')
            ->select('deed_of_incorporation.*')
            ->where('deed_of_incorporation.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'deedData' => $deed
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function managementVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $manage = DB::table('management')
            ->select('management.*')
            ->where('management.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'managementData' => $manage
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function attachmentVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $attach = DB::table('attachment')
            ->select('attachment.*')
            ->where('attachment.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'attachmentData' => $attach
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function ahliVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $ahli = DB::table('expert')
            ->select('expert.*')
            ->where('expert.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'ahliData' => $ahli
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function SPTVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $tax = DB::table('tax')
            ->select('tax.*')
            ->where('tax.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'SPTData' => $tax
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function experienceVendor(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'token' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                if($token_data){
                    $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                    $userakses = User::where('id', $userid)->first();
                    $username = $userakses->user_name;
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Token is not valid'
                    ], 401);
                }
            }

            $vendor = DB::table('vendor')
            ->select('vendor.*')
            ->where('vendor.USER_NAME', $username)
            ->get();

            // dd($vendor[0]->VENDOR_ID);

            $exp_vendor = DB::table('exp_vendor')
            ->select('exp_vendor.*')
            ->where('exp_vendor.vendor_id', $vendor[0]->VENDOR_ID)
            ->get();

            // dd(count($users));
            if(count($vendor)>0){
                return response()->json([
                    'experienceData' => $exp_vendor
                ], 200);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Not Found'
                ], 404);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    /**
     * Login The User
     * @param Request $request
     * @return User
     */
}