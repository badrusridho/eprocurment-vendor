<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Mail\sendEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    /**
     * Login The User
     * @param Request $request
     * @return User
     */
    public function loginUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                // 'email' => 'required|email',
                'user_name' => 'required',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $users = DB::table('users')
            ->join('vendor', 'users.user_name', '=', 'vendor.USER_NAME')
            ->select('users.group_users_id', 'vendor.CONFIRM')
            ->where('users.user_name', $request->user_name)
            ->get();
            
            // dd(count($users));
            if(count($users)>0){
                $grupusers=$users[0]->group_users_id;
            }else{
                $grupusers=0;
            }

            // dd($grupusers);

            if($grupusers <> 2){

                return response()->json([
                    'status' => false,
                    'message' => 'Username & Password does not match with our authentication.',
                ], 401);
            }

            if(!Auth::attempt($request->only(['user_name', 'password']))){
                return response()->json([
                    'status' => false,
                    'message' => 'Username & Password does not match with our record.',
                ], 401);
            }

            if($users[0]->CONFIRM <> 1){
                return response()->json([
                    'status' => false,
                    'message' => 'Please konfirm in your email',
                ], 401);
            }

            // $user = User::where('email', $request->email)->first();
            $user = User::where('user_name', $request->user_name)->first();

            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'token' => $user->createToken("API TOKEN")->plainTextToken
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function updatePass(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(), 
            [
                'newpassword' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            } 

            if($username == $request->newpassword){
                return response()->json([
                    'status' => false,
                    'message' => "password cant be same with username"
                ], 500);
            }

            User::where('id', $userid)
            ->update([
                'password' => Hash::make($request->newpassword)
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Password Updated Successfully'
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function updateNewPass(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(), 
            [
                'password' => 'required',
                'newpassword' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            if (isset($post_data['token'])) {
                [$id, $user_token] = explode('|', $post_data['token'], 2);
                $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
                $userid = $token_data->tokenable_id; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!
                $userakses = User::where('id', $userid)->first();
                $username = $userakses->user_name;
            } 
            // echo $request->user_name = $username;
            $datakirim = $request->only(['user_name', 'password']);
            $datakirim['user_name'] = $username;
            // print_r($datakirim);
            if(!Auth::attempt($datakirim)){
                return response()->json([
                    'status' => false,
                    'message' => 'Password does not match with our record.',
                ], 401);
            } else {
                User::where('id', $userid)
                ->update([
                    'password' => Hash::make($request->newpassword)
                ]);
                
                return response()->json([
                    'status' => true,
                    'message' => 'Password Updated Successfully'
                ], 200);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function ForgotPass(Request $request){        
        try {
            $validateUser = Validator::make($request->all(), 
            [
                'email' => 'required|email'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            $email = $post_data['email'];
            $user = User::where('email', $email)->first();
            // dd($user);
            if($user){
                $response = Mail::to($email)->send(new sendEmail($user));
                if($response)
                {
                    return response()->json([
                        'status' => true,
                        'norut' => $user['id'],
                        'message' => 'Email sent successfully.',
                    ]);
                }
                else
                {
                    return response()->json([
                        'status' => false,
                        'message' => 'Wrong Data'
                    ]);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Tidak Ditemukan'
                ], 404);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    
    public function resetPass(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(), 
            [
                'newpassword' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $post_data = $request->all();
            $userid = $post_data['norut']; // !!!THIS ID WE CAN USE TO GET DATA OF YOUR USER!!!


            User::where('id', $userid)
            ->update([
                'password' => Hash::make($request->newpassword)
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Password Updated Successfully'
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}