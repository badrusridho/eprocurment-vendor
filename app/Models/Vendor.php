<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Vendor extends Authenticatable
{
    use HasFactory, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'VENDOR_ID',
        'USER_NAME',
        'PASSWORD',
        'COMPANY_NAME',
        'NPWP',
        'STATUS',
        'ADDRESS',
        'CITY',
        'PROVINCE',
        'POSTAL_CODE',
        'PHONE',
        'FAX',
        'EMAIL',
        'BANK',
        'ACCOUNT_NUMBER',
        'WEBSITE',
        'PATH_FILE',
        'VARIFICATOR_DECISION',
        'VERIFICATOR_DATE',
        'VERIFICATOR_REMARK',
        'FINAL_DECISION',
        'FINAL_DATE',
        'FINAL_REMARK',
        'CREATED_DATE',
        'CREATED_BY',
        'MODIFIED_DATE',
        'MODIFIED_BY',
        'DELETED_DATE',
        'DELETED_BY',
        'CONFIRM'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
}
