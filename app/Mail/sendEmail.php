<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Attachment;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class sendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public $data;
    public $nama;
    public function __construct($data)
    {
        
        // dd($data);
        $this->data = $data;
        // dd($this->data->COMPANY_NAME);
        // dd($this->data);
        // Gabungkan nama menjadi satu string
        $this->nama = $data->COMPANY_NAME;
        
        // dd($this->data);
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Konfirmasi Pendaftaran a/n '.$this->nama,
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'templatemail',
            with: [
                'pemesan'       => $this->nama,
                'data'          => $this->data,
                'url'           => 'http://localhost/eprocurment-vendor/public/api/vendor/confirm/'.$this->data->VENDOR_ID
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    // public function attachments(): array
    // {
    //     $attachments = [];
    //     // $noFaktur = $this->data[0]->no_faktur;
    //     foreach ($this->data['no_faktur'] as $idx => $noFaktur) {
    //         # code...
    //         $filePath = public_path('assets/pdf/faktur/'.$noFaktur.'.pdf');
    //         if (file_exists($filePath)) {
    //             // dd('yes');
    //             $attachments[] = Attachment::fromPath($filePath);
    //         }
    //     }
    //     return $attachments;

        
    //     // Periksa apakah file ada
        
    // }

}
