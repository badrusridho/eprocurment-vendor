public function fakturEmail(Request $request){
        $data = $request->json()->all();
        // return($data["email"]);
        if($data["secretkey"] == "SAT123SU77")
        {
            $email = $data["email"];
            $response = Mail::to($email)->send(new sendEmail($data["RequestParameter"]));
            if($response)
            {
                return response()->json([
                    'success' => true,
                    'message' => 'Email sent successfully.',
                ]);
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Wrong Data'
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Not Authorize'
            ]);
        }
        
    }